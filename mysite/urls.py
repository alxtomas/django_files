from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

from .views import current_datetime
from .views import hello
from .views import hours_ahead
from .views import add
from .views import sub
from .views import product
from .views import square
from .views import welcome_math


urlpatterns = patterns('',
	url(r'^$', welcome_math),
	url(r'^add/(\d+)/(\d+)/$', add),
	url(r'^sub/(\d+)/(\d+)/$', sub),
	url(r'^product/(\d+)/(\d+)/$', product),
	url(r'^(\d+)/$', square),
    url(r'^hello/$', hello),
    url(r'^time/$', current_datetime),
    url(r'^time/plus/(\d+)/$', hours_ahead),
    url(r'^admin/', include(admin.site.urls)),
)