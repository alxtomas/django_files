from django.http import Http404, HttpResponse
import datetime
from django.shortcuts import render
from django.http import HttpResponse
from django.template import Context
from django.template.loader import get_template


def hello(request):
    return HttpResponse("Hello world")

def current_datetime(request):
 	now = datetime.datetime.now()
  	context = ({'current_date': now})
  	return render(
  		request,
  		'current_datetime.html',
  		context
  		)

def hours_ahead(request, offset):
    try:
        offset = int(offset)
    except ValueError:
        raise Http404()
    dt = {
    	'next_time': datetime.datetime.now() + 
    		datetime.timedelta(hours=offset),
    	'hour_offset': offset
    	}
    return render(request, 'hours_ahead.html', dt)

def welcome_math(request):
	return HttpResponse("Welcome to Math!")

def add(request, int1, int2):
	num1 = int(int1)
	num2 = int(int2)
	ans = num1 + num2
	html = "The sum of {} and {} is {}.".format(num1, num2, ans)

	return HttpResponse(html)

def sub(request, int1, int2):
	num1 = int(int1)
	num2 = int(int2)
	ans = num1 - num2
	html = "The difference of {} and {} is {}.".format(num1, num2, ans)

	return HttpResponse(html)

def product(request, int1, int2):
	num1 = int(int1)
	num2 = int(int2)
	ans = num1 + num2
	html = "The product of {} and {} is {}.".format(num1, num2, ans)

	return HttpResponse(html)

def square(request, offset):
	num = int(offset)
	square = num * num;
	return HttpResponse("The square of {} is {}".format(num, square))