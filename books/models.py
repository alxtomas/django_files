from __future__ import unicode_literals

from django.db import models
import datetime
from django.utils import timezone

# Create your models here.

class Publisher(models.Model):
	name = models.CharField(max_length=30)
	address = models.CharField(max_length=50)
	city = models.CharField(max_length=60)
	state_province = models.CharField(max_length=30)
	country = models.CharField(max_length=50)
	website = models.URLField()

	def __unicode__(self):
		return self.name

class Author(models.Model):
	first_name = models.CharField(max_length=30)
	last_name = models.CharField(max_length=40)
	email = models.EmailField(verbose_name="E-mail")

	def __unicode__(self):
		return "{} {}".format(self.first_name, self.last_name)

	def display_slug(self):
		slug = "{}-{}".format(self.first_name.lower().replace(" ","-"), self.last_name.lower().replace(" ","-"))
		return slug

class Book(models.Model):
	title = models.CharField(max_length=100)
	authors = models.ManyToManyField(Author)
	publisher = models.ForeignKey(Publisher)
	publication_date = models.DateField()

	def __unicode__(self):
		return self.title


	def was_published_recently(self):
		date_today = timezone.now().date()
		return self.publication_date >= date_today - datetime.timedelta(days=1)

	was_published_recently.boolean = True
	was_published_recently.short_description = "Published Recently?"