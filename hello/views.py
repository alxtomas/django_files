from django.http import Http404, HttpResponse
import datetime


def hello(request):
    return HttpResponse("Hello world")

def current_datetime(request):
    now = datetime.datetime.now()
    html = "<html><body>It is now %s.</body></html>" % now
    return HttpResponse(html)

def hours_ahead(request, offset):
    try:
        offset = int(offset)
    except ValueError:
        raise Http404()
    dt = datetime.datetime.now() + datetime.timedelta(hours=offset)
    html = "<html><body>In %s hour(s), it will be %s.</body></html>" % (offset, dt)
    return HttpResponse(html)

def welcome_math(request):
	return HttpResponse("Welcome to Math!")

def add(request, int1, int2):
	num1 = int(int1)
	num2 = int(int2)
	ans = num1 + num2
	html = "The sum of {} and {} is {}.".format(num1, num2, ans)

	return HttpResponse(html)

def sub(request, int1, int2):
	num1 = int(int1)
	num2 = int(int2)
	ans = num1 - num2
	html = "The difference of {} and {} is {}.".format(num1, num2, ans)

	return HttpResponse(html)

def product(request, int1, int2):
	num1 = int(int1)
	num2 = int(int2)
	ans = num1 + num2
	html = "The product of {} and {} is {}.".format(num1, num2, ans)

	return HttpResponse(html)

def square(request, offset):
	num = int(offset)
	square = num * num;
	return HttpResponse("The square of {} is {}".format(num, square))